package com.qjchen.server.model;


/**
 * 客户端注册model
 */
public class ClientRegisterModel {

    /**
     * 客户端-退出登录地址
     */
    private String loginOutUrl;

    /**
     * 客户端-jsessionid
     */
    private String jsessionid;

    public String getLoginOutUrl() {
        return loginOutUrl;
    }

    public void setLoginOutUrl(String loginOutUrl) {
        this.loginOutUrl = loginOutUrl;
    }

    public String getJsessionid() {
        return jsessionid;
    }

    public void setJsessionid(String jsessionid) {
        this.jsessionid = jsessionid;
    }
}