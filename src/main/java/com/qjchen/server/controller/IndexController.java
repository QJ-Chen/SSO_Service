package com.qjchen.server.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    /**
     * 跳转index页
     */
    @RequestMapping({"/", "index","login"})
    public String index() {
        return "login";
    }
}