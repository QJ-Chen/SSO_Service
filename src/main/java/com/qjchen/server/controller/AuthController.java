package com.qjchen.server.controller;

import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.exception.BusinessException;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.AuthDTO;
import com.qjchen.common.model.dto.PlateDTO;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.entity.Auth;
import com.qjchen.common.model.vo.AuthVO;
import com.qjchen.common.utils.ConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("${qjc.sso.api}/auth")
public class AuthController extends BusinessController<AuthVO, AuthDTO, Auth> {

    @Autowired
    private PlateController plateController;

    @Autowired
    private RoleController roleController;

    @Override
    public Page<AuthVO> listPage(@RequestBody AuthDTO authDTO) {
        int currentPageNo = authDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            authDTO.setCurrentPageNo(authDTO.getPageSize()*authDTO.getCurrentPageNo());
        }
        List<AuthVO> authVOS = new ArrayList<>();
        super.serviceAdapter.list(authDTO).forEach(authDTOTemp -> {
            AuthVO authVO = ConvertUtil.fusion(authDTOTemp, AuthVO.class);
            authVO.setRole(roleController.get(new RoleDTO(authDTOTemp.getRoleId())));
            authVO.setPlate(plateController.get(new PlateDTO(authDTOTemp.getPlateId())));
            authVOS.add(authVO);
        });
        int count = super.serviceAdapter.count(authDTO);
        return new Page<>(authVOS.size()/authDTO.getPageSize(),currentPageNo,authDTO.getPageSize(),count,authVOS);
    }

    @Override
    public List<AuthVO> list(@RequestBody AuthDTO authDTO) {
        List<AuthVO> authVOS = new ArrayList<>();
        super.serviceAdapter.list(authDTO).forEach(authDTOTemp -> {
            AuthVO authVO = ConvertUtil.fusion(authDTOTemp, AuthVO.class);
            authVO.setRole(roleController.get(new RoleDTO(authDTOTemp.getRoleId())));
            authVO.setPlate(plateController.get(new PlateDTO(authDTOTemp.getPlateId())));
            authVOS.add(authVO);
        });
        return authVOS;
    }

    @Override
    public AuthVO get(@RequestBody AuthDTO authDTO) {
        AuthDTO authDTOTemp = super.serviceAdapter.get(authDTO);
        if(authDTOTemp==null){
            throw new BusinessException("未找到该权限");
        }
        return ConvertUtil.fusion(authDTOTemp, AuthVO.class);
    }
}
