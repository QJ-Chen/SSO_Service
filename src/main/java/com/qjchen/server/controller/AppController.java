package com.qjchen.server.controller;

import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.model.annotations.ExclusionAuthority;
import com.qjchen.common.model.annotations.ResModel;
import com.qjchen.common.config.RedisConstants;
import com.qjchen.common.interceptor.Result;
import com.qjchen.common.interceptor.ResultEnum;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.common.model.dto.PlateDTO;
import com.qjchen.common.model.vo.AppVO;
import com.qjchen.common.model.vo.PlateVO;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.server.core.utils.jwtUtils.JwtUtils;
import com.qjchen.server.core.utils.redisUtils.RedisPoolUtil;
import com.qjchen.server.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("${qjc.sso.api}/app")
public class AppController extends BusinessController<AppVO, AppDTO,Void> {

    @Autowired
    private AppService appService;

    @Autowired
    private PlateController plateController;

    @Override
    public List<AppVO> list(@RequestBody AppDTO appDTO) {
        if (!RedisPoolUtil.heixists(RedisConstants.SSO_SERVICE_REDIS_TEMP,"app_list_temp")){
            List<AppDTO> list = appService.list(appDTO);

            Map<String,String> appMap = new HashMap<>();
            appMap.put("appTemp"+ JSONObject.toJSONString(appDTO.getIds()),JSONObject.toJSONString(list));
            RedisPoolUtil.hmset(RedisConstants.SSO_SERVICE_REDIS_TEMP,appMap);

            return ConvertUtil.listFusion(list,AppVO.class);
        }else{
            return RedisPoolUtil.hget(RedisConstants.SSO_SERVICE_REDIS_TEMP,"appTemp"+JSONObject.toJSONString(appDTO.getIds()),List.class);
        }

    }

    @Override
    public AppVO get(@RequestBody AppDTO appDTO) {
        AppDTO appDTOTemp = appService.get(appDTO);
        AppVO appVO = ConvertUtil.fusion(appDTOTemp, AppVO.class);
        appVO.setPlate(plateController.get(new PlateDTO(appDTOTemp.getPlateId())));
        return appVO;
    }

    @ResModel
    @ExclusionAuthority
    @PostMapping("/register")
    public Result<String> register(@RequestBody AppDTO appDTO){
        AppDTO appDTOTemp = appService.get(appDTO);
        if (appDTOTemp != null){
            if (appDTOTemp.getAppSignature().equals(appDTO.getAppSignature())){
                PlateVO plateVO = plateController.get(new PlateDTO(appDTOTemp.getPlateId()));
                if (plateVO.getStatus().getId() == 0){
                    return new Result<>(ResultEnum.FORBIDDEN.getCode(), ResultEnum.FORBIDDEN.getMessage(),"应用所属平台已禁用，请启用平台后再尝试启动应用。");
                }
                String jwtToken = JwtUtils.getJwtToken(appDTOTemp.getId(), appDTOTemp.getAppName());
                Map<String,String> appMap = new HashMap<>();
                Map<String,Object> appTemp = new HashMap<>();
                appTemp.put("appToken",jwtToken);
                appTemp.put("appInfo",ConvertUtil.fusion(appDTOTemp, AppVO.class));
                appMap.put("appTicket_"+appDTO.getAppCode(),JSONObject.toJSONString(appTemp));
                RedisPoolUtil.hmset(RedisConstants.APP_TICKET_REDIS_TEMP,appMap);
                return new Result<>(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getMessage(), jwtToken);
            }
        }
        return new Result<>(ResultEnum.FORBIDDEN.getCode(), ResultEnum.FORBIDDEN.getMessage(),"应用未注册。");
    }

    @ResModel
    @PostMapping("/listAppName")
    public List<String> listAppName(@RequestBody AppDTO appDTO){
        List<AppDTO> list = appService.list(appDTO);
        return list.stream().map(AppDTO::getAppName).collect(Collectors.toList());
    }
}
