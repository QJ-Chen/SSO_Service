package com.qjchen.server.controller;

import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.config.PropertiesConfig;
import com.qjchen.common.exception.BusinessException;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.annotations.ExclusionAuthority;
import com.qjchen.common.model.annotations.ResModel;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.dto.UserDTO;
import com.qjchen.common.model.entity.User;
import com.qjchen.common.model.vo.AppVO;
import com.qjchen.common.model.vo.RoleVO;
import com.qjchen.common.model.vo.UserVO;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.common.utils.CookieUtil;
import com.qjchen.common.utils.StringUtils;
import com.qjchen.server.core.utils.jwtUtils.JwtUtils;
import com.qjchen.server.core.utils.redisUtils.RedisPoolUtil;
import com.qjchen.server.model.ClientRegisterModel;
import com.qjchen.server.model.SSOConstantPool;
import com.qjchen.server.model.annotation.SDK;
import com.qjchen.server.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("${qjc.sso.api}/user")
public class UserController extends BusinessController<UserVO,UserDTO, User> {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private RoleController roleController;

    @Autowired
    private AppController appController;

    @SDK
    @Override
    @ExclusionAuthority
    public Page<UserVO> listPage(@RequestBody UserDTO userDTO) {
        HttpServletRequest request = super.requestService.getCurrentRequest();
        AppDTO appDTO = new AppDTO();
        appDTO.setAppCode(request.getHeader("appCode"));
        AppVO appVO = appController.get(appDTO);
        userDTO.setAppIds(String.valueOf(appVO.getId()));
        int currentPageNo = userDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            userDTO.setCurrentPageNo(userDTO.getPageSize()*userDTO.getCurrentPageNo());
        }
        List<UserVO> userVOList = new ArrayList<>();
        super.serviceAdapter.list(userDTO).forEach(userDTOTemp -> {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(userDTOTemp,userVO);

            RoleVO roleVO = roleController.get(new RoleDTO(userDTOTemp.getRoleId()));
            userVO.setRoleVO(roleVO);

            List<AppVO> list = appController.list(new AppDTO(userDTOTemp.getAppIds()));
            userVO.setAppVOList(list);

            userVOList.add(userVO);
        });
        int count = super.serviceAdapter.count(userDTO);
        return new Page<>(userVOList.size()/userDTO.getPageSize(),currentPageNo,userDTO.getPageSize(),count,userVOList);
    }

    /**
     * 认证中心SSO统一登录方法
     */
    @ExclusionAuthority
    @RequestMapping("/login")
    public String login(UserDTO userDTO, HttpSession session, Model model, HttpServletResponse response,HttpServletRequest request) {
        if (StringUtils.isEmpty(userDTO.getUsername())){
            model.addAttribute("msg", "请输入用户名密码后再登录！");
            model.addAttribute("redirectUrl", userDTO.getRedirectUrl());
            return "login";
        }

        UserDTO userInfo = userService.get(userDTO);
        if (userInfo == null){
            model.addAttribute("msg", "输入用户未注册！");
            model.addAttribute("redirectUrl", userDTO.getRedirectUrl());
            return "login";
        }
        if (RedisPoolUtil.heixists("app_ticket_temp","appTicket_"+CookieUtil.getCookie(request,"appCode"))){
            Map appTicketTemp = RedisPoolUtil.hget("app_ticket_temp", "appTicket_" + CookieUtil.getCookie(request, "appCode"), Map.class);
            AppVO appVO = JSONObject.parseObject(JSONObject.toJSONString(appTicketTemp.get("appInfo")), AppVO.class);

            if (!userInfo.getAppIds().contains(String.valueOf(appVO.getId()))){
                model.addAttribute("msg", "当前用户没有权限访问该应用，请尝试切换用户登录！");
                model.addAttribute("redirectUrl", userDTO.getRedirectUrl());
                return "login";
            }
        }
        if (userInfo != null && !DigestUtils.sha256Hex(userDTO.getPassword()).equals(userInfo.getPassword())){
            model.addAttribute("msg", "账户或密码错误，请重新登录!");
            model.addAttribute("redirectUrl", userDTO.getRedirectUrl());
            return "login";
        }

        //创建令牌
        String ssoToken = JwtUtils.getJwtToken(userInfo.getId(), userInfo.getUsername());

        //把令牌放到全局会话中
        session.setAttribute(PropertiesConfig.TOKEN_NAME, ssoToken);
        session.setAttribute("ID",userInfo.getId());
        session.setAttribute("isLogin",true);
        session.setMaxInactiveInterval(3600);

        //完善用户绑定信息
        Map<String,String> map = new HashMap<>();
        UserVO userVO = ConvertUtil.fusion(userInfo, UserVO.class);
        RoleVO roleVO = roleController.get(new RoleDTO(userInfo.getRoleId()));
        userVO.setRoleVO(roleVO);
        userVO.setAppVOList(appController.list(new AppDTO(userInfo.getAppIds())));
        userVO.setStatus(userInfo.getStatus());

        //登录凭证信息缓存
        map.put(ssoToken, JSONObject.toJSONString(userVO));
        RedisPoolUtil.hmset(PropertiesConfig.TOKEN_NAME+userInfo.getId(),map);
        RedisPoolUtil.exprire(PropertiesConfig.TOKEN_NAME+userInfo.getId(),3600);

        //未携带重定向跳转地址-默认跳转到认证中心首页
        if (StringUtils.isEmpty(userDTO.getRedirectUrl())) {
            return "login";
        }

        //设置cookie
        CookieUtil.setCookie(response,PropertiesConfig.TOKEN_NAME,ssoToken,3600);
        CookieUtil.setCookie(response,"ID", String.valueOf(userInfo.getId()),3600);


        log.debug("[ SSO登录 ] login success SSO_TOKEN:{} , sessionId:{}", ssoToken, session.getId());
        // 跳转到客户端
        return "redirect:" + userDTO.getRedirectUrl();
    }

    /**
     * 校验令牌是否合法
     *
     * @param ssoToken    令牌
     * @param loginOutUrl 退出登录访问地址
     * @param jsessionid
     * @return 令牌是否有效
     */
    @ResponseBody
    @ExclusionAuthority
    @RequestMapping("/checkToken")
    public Boolean verify(String ssoToken, String loginOutUrl, String jsessionid,String id) {
        // 判断token是否存在map容器中,如果存在则代表合法
        boolean isVerify = RedisPoolUtil.heixists(PropertiesConfig.TOKEN_NAME+id,ssoToken);
        if (!isVerify) {
            log.debug("[ SSO-令牌校验 ] checkToken 令牌已失效 SSO_TOKEN:{}", ssoToken);
            return false;
        }

        //把客户端的登出地址记录起来,后面注销的时候需要根据使用(生产环境建议存库或者redis)
        List<ClientRegisterModel> clientInfoList = SSOConstantPool.CLIENT_REGISTER_POOL.computeIfAbsent(ssoToken, k -> new ArrayList<>());
        ClientRegisterModel vo = new ClientRegisterModel();
        vo.setLoginOutUrl(loginOutUrl);
        vo.setJsessionid(jsessionid);
        clientInfoList.add(vo);
        log.debug("[ SSO-令牌校验 ] checkToken success SSO_TOKEN:{} , clientInfoList:{}", ssoToken, clientInfoList);
        return true;
    }

    /**
     * 校验是否已经登录认证中心（是否有全局会话）
     * 1.若存在则携带令牌ssoToken跳转至目标页面
     * 2.若不存在则跳转到登录页面
     */
    @ExclusionAuthority
    @RequestMapping("/checkLogin")
    public String checkLogin(String redirectUrl, Model model, HttpServletRequest request,HttpServletResponse response) {
        //从认证中心-session中判断是否已经登录过(判断是否有全局会话)
        String id = String.valueOf(request.getSession().getAttribute("ID"));
        String ssoToken = (String) request.getSession().getAttribute(PropertiesConfig.TOKEN_NAME+id);
        boolean isCheckLogin = false;
        if (ssoToken != null && id != null)
            isCheckLogin = RedisPoolUtil.heixists(PropertiesConfig.TOKEN_NAME+id,ssoToken);
        // ssoToken为空 - 没有全局回话
        if (!isCheckLogin) {
            log.debug("[ SSO-登录校验 ] checkLogin fail 没有全局回话 SSO_TOKEN:{}", ssoToken);
            //登录成功需要跳转的地址继续传递
            model.addAttribute("redirectUrl", redirectUrl);
            CookieUtil.setCookie(response,"appCode",request.getParameter("appCode"),3600);
            //跳转到统一登录页面
            return "login";
        }
        // 重制令牌有效期
        RedisPoolUtil.exprire(PropertiesConfig.TOKEN_NAME+id,3600);
        log.debug("[ SSO-登录校验 ] checkLogin success 有全局回话  SSO_TOKEN:{}", ssoToken);
        // 携带令牌到客户端 TODO 原逻辑是直接拼接到URL上，后改为使用Cookie，跨域问题后期解决
        // redirectAttributes.addAttribute("ssoToken", ssoToken);
        CookieUtil.setCookie(response,PropertiesConfig.TOKEN_NAME,ssoToken,3000);
        //重定向到目标系统
        return "redirect:" + redirectUrl;
    }

    /**
     * 统一注销
     * 1.注销全局会话
     * 2.通过监听全局会话session时效性，向已经注册的所有子系统发起注销请求
     */
    @ResModel
    @ExclusionAuthority
    @RequestMapping("/logOut")
    public String logOut(HttpServletRequest request) {
        String id = request.getHeader("id");
        String ssoToken = request.getHeader("token");
        RedisPoolUtil.hmRemove(PropertiesConfig.TOKEN_NAME+id,ssoToken);
        return "已退出登录";
    }

    @ResModel
    @ExclusionAuthority
    @PostMapping("/getByToken")
    public UserVO getByToken(HttpServletRequest request){
        String token = request.getHeader("token");
        String id = request.getHeader("id");
        if (RedisPoolUtil.heixists(PropertiesConfig.TOKEN_NAME+id,token)){
            return RedisPoolUtil.hget(PropertiesConfig.TOKEN_NAME + id, token, UserVO.class);
        }
        throw new BusinessException("登录已过期");
    }

    @RequestMapping("/register/view")
    public String registerView(){
        return "register";
    }

    @Override
    public UserVO get(@RequestBody UserDTO userDTO) {
        UserDTO userDTOTemp = super.serviceAdapter.get(userDTO);
        UserVO userVO = ConvertUtil.fusion(userDTOTemp, UserVO.class);

        RoleVO roleVO = roleController.get(new RoleDTO(userDTOTemp.getRoleId()));
        userVO.setRoleVO(roleVO);

        List<AppVO> list = appController.list(new AppDTO(userDTOTemp.getAppIds()));
        userVO.setAppVOList(list);
        return userVO;
    }
}
