package com.qjchen.server.controller;

import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.vo.RoleVO;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.server.service.RoleService;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 角色控制层
 * @author qjchen
 * @date 2023.7.29
 */
@RestController
@RequestMapping("${qjc.sso.api}/role")
public class RoleController extends BusinessController<RoleVO, RoleDTO,Void> {

    @Autowired
    private RoleService roleService;

    @Override
    public Page<RoleVO> listPage(@RequestBody RoleDTO roleDTO) {
        int currentPageNo = roleDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            roleDTO.setCurrentPageNo(roleDTO.getPageSize()*roleDTO.getCurrentPageNo());
        }
        List<RoleDTO> list = roleService.list(roleDTO);
        List<RoleVO> roleVOList = ConvertUtil.listFusion(list, RoleVO.class);
        return new Page<>(roleVOList.size()/roleDTO.getPageSize(),currentPageNo,roleDTO.getPageSize(),roleVOList.size(),roleVOList);
    }

    @Override
    public RoleVO get(@RequestBody RoleDTO roleDTO) {
        RoleDTO roleDTOTemp = roleService.get(roleDTO);
        RoleVO roleVO = new RoleVO();
        BeanUtils.copyProperties(roleDTOTemp,roleVO);
        return roleVO;
    }

}
