package com.qjchen.server.service;

import com.qjchen.common.api.service.BusinessService;
import com.qjchen.common.model.dto.AuthDTO;

public interface AuthService extends BusinessService<AuthDTO> {
}
