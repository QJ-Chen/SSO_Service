package com.qjchen.server.service.impl;

import com.qjchen.common.api.service.BusinessServiceAdapter;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.entity.Role;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.common.utils.ResUtils;

import com.qjchen.server.mapper.RoleMapper;
import com.qjchen.server.service.RoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl extends BusinessServiceAdapter<Role,RoleDTO> implements RoleService {

}
