package com.qjchen.server.core;

import com.qjchen.common.config.PropertiesConfig;
import com.qjchen.common.interceptor.Result;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.qas.sso.SSOClientConfig;
import com.qjchen.server.controller.AppController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class SSOServiceAppRunner implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(SSOServiceAppRunner.class);

    @Autowired
    private AppController appController;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Result<String> register = appController.register(new AppDTO(SSOClientConfig.APP_CODE, SSOClientConfig.APP_SIGNATURE));
        if (register.getCode() == 200){
            PropertiesConfig.APP_TOKEN = register.getData();
        }
    }
}
