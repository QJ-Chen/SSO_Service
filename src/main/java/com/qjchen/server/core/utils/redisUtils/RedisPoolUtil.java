package com.qjchen.server.core.utils.redisUtils;

import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.exception.ForbiddenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class RedisPoolUtil{

    private static Logger log = LoggerFactory.getLogger(RedisPoolUtil.class);

    /**
     * jedis set方法，通过设置值过期时间exTime,单位:秒<br>
     * 为后期session服务器共享，Redis存储用户session所准备
     *
     * @param key    key
     * @param value  value
     * @param exTime 过期时间,单位:秒
     * @return 执行成功则返回result 否则返回null
     */
    public static String setEx(String key, String value, int exTime){
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.setex(key, exTime, value);
        }catch (Exception e){
            log.error("set key:{} value{} error",key,value,e);
            return result;
        }finally {
            if (jedis!=null){
                jedis.close();
            }
        }
        return result;
    }


    /**
     * 对key所对应的值进行重置过期时间expire
     *
     * @param key    key
     * @param exTime 过期时间 单位:秒
     * @return 返回重置结果, 1:时间已经被重置，0:时间未被重置
     */
    public static Long expire(String key, int exTime) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.expire(key, exTime);
        } catch (Exception e) {
            log.error("expire key:{} error ", key, e);
            return result;
        }finally {
            jedis.close();
        }
        return result;

    }

    /**
     * jedis set方法
     *
     * @param key   key
     * @param value value
     * @return 执行成功则返回result，否则返回null
     */
    public static String set(String key, String value) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.set(key, value);
        } catch (Exception e) {
            log.error("set key:{} value{} error", key, value, e);
            return result;
        }finally {
            jedis.close();
        }
        return result;
    }

    /**
     * jedis get方法
     *
     * @param key key
     * @return 返回key对应的value 异常则返回null
     */
    public static String get(String key) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.get(key);
        } catch (Exception e) {
            log.error("set key:{}error", key, e);
            return result;
        }finally {
            jedis.close();
        }
        return result;
    }

    /**
     * jedis 删除方法
     *
     * @param key key
     * @return 返回结果，异常返回null
     */
    public static Long del(String key) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.del(key);
        } catch (Exception e) {
            log.error("del key:{} error", key, e);
            return result;
        }finally {
            jedis.close();
        }
        return result;
    }

    public static void sadd(String key,String value){
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            jedis.sadd(key,value);
        }catch (Exception e){
            log.error("sadd value:{} error",value,e);
        }finally {
            jedis.close();
        }
    }

    public static boolean sismember(String key,String value){
        Jedis jedis = null;
        AtomicBoolean isSismember = new AtomicBoolean(false);
        try {
            jedis = RedisPool.getJedis();
            if(value != null){
                Set<String> smembers = jedis.smembers(key);
                smembers.forEach(s -> {
                    if (s.contains(value))
                        isSismember.set(true);
                });
            }
            return isSismember.get();
        }catch (Exception e){
            log.error("sismember key{}:value{} error",key,value,e);
            return false;
        }finally {
            jedis.close();
        }
    }

    /**
     * jedis Map
     *
     * @param key   key
     * @return 执行成功则返回result，否则返回null
     */
    public static String hmset(String key, Map<String,String> value) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hmset(key, value);
        } catch (Exception e) {
            log.error("set key:{} error", key, e);
            return result;
        }finally {
            jedis.close();
        }
        return result;
    }

    /**
     * 设置key的过期时间
     * @param key 键名
     * @param exTime 过期时间（秒）
     */
    public static void exprire(String key, int exTime){
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            jedis.expire(key,exTime);
        }catch (Exception e){
            log.error("expire key:{} error",key,e);
        }finally {
            jedis.close();
        }
    }

    /**
     * 删除Map中指定一条数据
     *
     * @return Boolean
     */
    public static long hmRemove(String key, String value){
        Jedis jedis = null;
        long result = 0L;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hdel(key,value);
        } catch (Exception e) {
            log.error("set key:{} error", key, e);
            return result;
        }finally {
            jedis.close();
        }
        return result;
    }

    /**
     * jedis 获取某条数据
     *
     * @param key   key
     * @return 执行成功则返回result，否则返回null
     */
    public static <R> R hget(String key,String field,Class<R> targetClass) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hget(key,field);
        } catch (Exception e) {
            log.error("set key:{} error", key, e);
            throw new ForbiddenException("系统缓存错误");
        }finally {
            jedis.close();
        }
        return JSONObject.parseObject(result,targetClass);
    }

    /**
     * jedis 验证hash中是否存在某条数据
     *
     * @param key   key
     * @return 执行成功则返回result，否则返回null
     */
    public static Boolean heixists(String key,String field) {
        Jedis jedis = null;
        Boolean result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hexists(key, field);
        } catch (Exception e) {
            log.error("set key:{} error", key, e);
            return result;
        }finally {
            jedis.close();
        }
        return result;
    }
}

