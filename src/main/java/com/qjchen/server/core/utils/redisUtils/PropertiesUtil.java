package com.qjchen.server.core.utils.redisUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * 读取properties配置文件的工具类
 */
public class PropertiesUtil {
    /**
     * 读取日志
     */
    private static Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);

    /**
     * properties属性
     */
    private static Properties props;

    static {
        String fileName = "application.properties";
        props = new Properties();
        try {
            props.load(
                    new InputStreamReader(
                            PropertiesUtil.class
                                    .getClassLoader()
                                    .getResourceAsStream(fileName),"UTF-8"));

        }catch (IOException e){
            logger.error("配置文件读取异常");
        }
    }

    /**
     * 获取配置文件当中key所对应值
     *
     * @param key key
     * @return 返回key所对应的值，存在则返回，不存在则返回null
     */
    public static String getProperty(String key) {
        String value = props.getProperty(key.trim());
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return value.trim();
    }


    /**
     * 获取配置文件当中key所对应值，存在则进行数字转换。将其返回
     *
     * @param key key
     * @return 返回key对应的值，不存在或发生NumberFormatException则返回null
     */
    public static Integer getIntegerProperty(String key) {
        String value = props.getProperty(key.trim());
        Integer result;
        try {
            result = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            logger.info("参数转换异常：" + e.getMessage());
            return null;
        }
        return result;
    }


    /**
     * 获取配置文件当中key所对应值，存在则进行数字转换。将其返回
     *
     * @param key          key
     * @param defaultValue 默认值
     * @return 返回key对应的值，不存在时或发生NumberFormatException则返回defaultValue
     */
    public static Integer getIntegerProperty(String key, Integer defaultValue) {
        String value = props.getProperty(key);
        Integer result;
        if (StringUtils.isEmpty(value)) {
            result = defaultValue;
            return  result;
        }
        try {
            result = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            logger.info("参数转换异常：" + e.getMessage());
            result = defaultValue;
        }
        return result;
    }

    /**
     * 获取配置文件当中key所对应值<br>
     * 如果其值为true则返回true，false或其他值则返回false,为空时返回null
     *
     * @param key key
     * @return key对应的值，值为true时返回true，false或其他值则返回false
     */
    public static Boolean getBooleanProperty(String key) {
        String value = props.getProperty(key.trim()).trim();
        Boolean result;
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        result = "true".equals(value);
        return result;
    }

    /**
     * 获取配置文件当中key所对应值<br>
     * 如果其值为true则返回true，false或其他值则返回false，为null时返回defaultValue
     *
     * @param key          key
     * @param defaultValue 默认值
     * @return key对应的值，值为true时返回true，false或其他值则返回false
     */
    public static Boolean getBooleanProperty(String key, Boolean defaultValue) {
        String value = props.getProperty(key.trim()).trim();
        Boolean result;
        if (StringUtils.isEmpty(value)) {
            result = defaultValue;
            return  result;
        }
        result = "true".equals(value);
        return result;
    }




}

