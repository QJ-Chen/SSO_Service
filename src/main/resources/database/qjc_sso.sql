/*
 Navicat Premium Data Transfer

 Source Server         : 本地DB
 Source Server Type    : MySQL
 Source Server Version : 80036 (8.0.36)
 Source Host           : 192.168.177.128:3306
 Source Schema         : qjc_sso

 Target Server Type    : MySQL
 Target Server Version : 80036 (8.0.36)
 File Encoding         : 65001

 Date: 30/06/2024 00:08:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sso_app
-- ----------------------------
DROP TABLE IF EXISTS `sso_app`;
CREATE TABLE `sso_app`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Id\n',
  `appCode` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '应用编码\n',
  `appName` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '应用名\n',
  `appSignature` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '应用签名\n',
  `createdBy` int NULL DEFAULT NULL COMMENT '创建者',
  `plateId` int NOT NULL COMMENT '所属平台id',
  `port` int NULL DEFAULT 8080 COMMENT '应用端口',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sso_app
-- ----------------------------
INSERT INTO `sso_app` VALUES (1, 'Test-App', '测试应用', 'fc45e10d237e5d405024e4bdd0fce5bfdb517350e013ad8af86bb856579c656d', 1, 1, 8080);
INSERT INTO `sso_app` VALUES (2, 'SSO-Manage', 'SSO管理平台', '19b26a0e565473b3f39fe58770b31d0cfbd031d78e0c9dd4c2615e8ab8d1163d', 2, 2, 8081);
INSERT INTO `sso_app` VALUES (3, 'SSO-Service', 'SSO服务端', '1634795b7969af096cd1d63a02630f964156a80f3470e70facbaa106430a18e7', 1, 2, 8010);

-- ----------------------------
-- Table structure for sso_auth
-- ----------------------------
DROP TABLE IF EXISTS `sso_auth`;
CREATE TABLE `sso_auth`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `plateId` int NOT NULL COMMENT '平台ID',
  `roleId` int NOT NULL COMMENT '角色ID',
  `insert` int NOT NULL COMMENT '添加权限(1为允许，0为禁止)',
  `update` int NOT NULL COMMENT '修改权限(1为允许，0为禁止)',
  `select` int NOT NULL COMMENT '查询权限(1为允许，0为禁止)',
  `delete` int NOT NULL COMMENT '删除权限(1为允许，0为禁止)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sso_auth
-- ----------------------------
INSERT INTO `sso_auth` VALUES (1, 2, 1, 1, 1, 1, 1);

-- ----------------------------
-- Table structure for sso_menu
-- ----------------------------
DROP TABLE IF EXISTS `sso_menu`;
CREATE TABLE `sso_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '菜单名',
  `parent` int NOT NULL COMMENT '父级id',
  `path` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '路径地址',
  `icon` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '图标',
  `type` int NULL DEFAULT 0 COMMENT '0为菜单，1为按钮',
  `component` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '组件地址',
  `perms` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '菜单标识',
  `appId` int NULL DEFAULT NULL COMMENT '所属应用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sso_menu
-- ----------------------------
INSERT INTO `sso_menu` VALUES (1, '用户中心', 1, '../views/user/UserIndex.vue', '1', 0, '/user/index', 'UserIndex', 2);

-- ----------------------------
-- Table structure for sso_plate
-- ----------------------------
DROP TABLE IF EXISTS `sso_plate`;
CREATE TABLE `sso_plate`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `plateCode` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '平台编码',
  `plateName` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '平台名称',
  `status` int NOT NULL DEFAULT 0 COMMENT '平台状态(0:未启用，1:启用)',
  `plateType` int NOT NULL COMMENT '平台类型(0:开发平台，1:测试平台，2:生产平台)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sso_plate
-- ----------------------------
INSERT INTO `sso_plate` VALUES (1, 'default', '默认平台', 0, 0);
INSERT INTO `sso_plate` VALUES (2, 'dev-manage', '统一认证管理平台', 1, 2);

-- ----------------------------
-- Table structure for sso_role
-- ----------------------------
DROP TABLE IF EXISTS `sso_role`;
CREATE TABLE `sso_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `roleCode` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '角色编码',
  `roleName` varchar(25) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '角色名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sso_role
-- ----------------------------
INSERT INTO `sso_role` VALUES (1, 'Sys_Admin', '系统管理员');
INSERT INTO `sso_role` VALUES (2, 'Sup_Admin', '超级管理员');
INSERT INTO `sso_role` VALUES (3, 'No_Auth', '未授权用户');
INSERT INTO `sso_role` VALUES (4, 'User_Act', '普通用户');

-- ----------------------------
-- Table structure for sso_user
-- ----------------------------
DROP TABLE IF EXISTS `sso_user`;
CREATE TABLE `sso_user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '密码(加密)',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `tel` varchar(25) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `roleId` int NULL DEFAULT NULL COMMENT '角色ID',
  `appIds` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '所属应用ID',
  `status` int NULL DEFAULT 1 COMMENT '状态（0:禁用，1:启用，默认0）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sso_user
-- ----------------------------
INSERT INTO `sso_user` VALUES (1, 'admin', '5e285c8c9e4cd8e8295b24464b174a403f27d46d19b4e5fc75547618f1962d69', '管理员', '000-0000000', 'q_j_chen@163.com', 1, '1,2', 1);
INSERT INTO `sso_user` VALUES (2, 'QJ_Chen', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '陈治民', '18230936053', '1140746032@163.com', 2, '1,2,3,4', 1);
INSERT INTO `sso_user` VALUES (3, '测试员', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '测试员', '0000-0000000', 'test@qjchen.com', 2, '1,2', 1);

SET FOREIGN_KEY_CHECKS = 1;
